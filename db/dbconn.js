const {Pool} = require('pg')
var config = require('./config')
const client = new Pool(config)
client.connect()

module.exports = client