
exports.up = function(knex) {
    return knex.schema
    .createTable('stations', function (table) {
       table.increments('id');
       table.decimal('lat',8,6);
       table.decimal('lng',8,6);
       table.string('name',20);
       table.string('address',100);
       table.integer('owner_id');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("stations")
};
