
exports.up = function(knex) {
    return knex.schema
    .createTable('users', function (table) {
       table.increments('id');
       table.string('uname',20);
       table.string('pass',100);
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("users")
};
