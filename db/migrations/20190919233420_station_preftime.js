
exports.up = function(knex) {
    return knex.schema
    .createTable('station_preftime', function (table) {
        table.increments('id')
        table.integer('station_id');
        table.time('preftime'); //Model hh/mm
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("station_preftime")
};
