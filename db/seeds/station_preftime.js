
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('station_preftime').del()
    .then(function () {
      // Inserts seed entries
      return knex('station_preftime').insert([
        {station_id: 2,preftime:'14:00'},
        {station_id: 2,preftime:'12:00'},
        {station_id: 2,preftime:'13:00'},
        {station_id: 2,preftime:'10:00'}

      ]);
    });
};
