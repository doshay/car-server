
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('stations').del()
    .then(function () {
      // Inserts seed entries
      return knex('stations').insert([
        {id:1,lat: 32.209490, lng: 34.904400,name:"הבית של אבא",address:"דרך ההדרים 84 שדה ורבורג",owner_id:1},
        {id:2,lat: 32.162651, lng: 34.898689,name:"הבית של אמא",address:"התחיה 5 הוד השרון",owner_id:1}
      ]);
    });
};
  