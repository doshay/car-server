var express = require('express');
var routes = require('./routes/routes');
var path = require('path')
var bodyParser = require('body-parser')

var app = express()

app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine' ,'hbs');
app.set('view options');
app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json());

app.use('/', routes);

app.listen(3000,function(){
  console.log("API running on port 3000")
})


