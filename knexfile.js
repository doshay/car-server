// Update with your config settings.
var client = require('./db/config')
module.exports = {

  
  development: {
    client: 'postgresql',
    connection: client,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    }
    
  },

  production: {
    client: 'postgresql',
    connection: client,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    }
  }

};
