var router = require('express').Router();
var stations_route = require('./stations');
var login_route = require('./login')


router.use('/api/stations',stations_route);
router.use('/api/login',login_route)
router.use('/api/users/:id',function(req,res){
    res.send(req.params.id)
})
module.exports = router