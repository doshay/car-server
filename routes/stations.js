var client = require('../db/dbconn')
var router = require('express').Router();



router.get('/',function(req,res){
   console.log("Request aaaaa")
    client.query("SELECT * FROM stations",null,function(err,rep){
         if(err){
             console.error(err.stack)
             res.status(404).end();
         }else{
             res.send(rep.rows);
         }
    })
 })
router.post('/',function(req,res){
    let r = req.body;
    let data = [r.lat,r.lng,r.name,r.address,r.owner_id]
    client.query("INSERT INTO stations(lat,lng,name,address,owner_id) VALUES($1,$2,$3,$4,$5)",data,function(err,rep){
        if(err){
            res.status(400).send(err.stack)
        }else{
            res.send("Station inserted.")
        }
    })
})
router.get('/:id',function(req,res){
    console.log("request dry info on station number " + req.params.id)
    client.query("SELECT * FROM stations WHERE id = "+req.params.id,null,function(err,rep){
        if(err){
            console.error(err.stack)
        }else{
            res.send(rep.rows[0])
        }
    })

    
})
router.get('/:id/wtime',function(req,res){
    console.log("request station hours on station number " + req.params.id)

    client.query("SELECT preftime FROM station_preftime WHERE station_id = "+req.params.id,null,function(err,rep){
        if(err){
            console.error(err.stack)
        }else{
            res.send(rep.rows)
        }
    })

    
})
module.exports = router